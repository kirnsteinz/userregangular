import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private api:ApiService) { }
  userlist:Object[];
  ngOnInit() {
    this.api.validate();
    this.api.fetch().subscribe(result => this.userlist = result);
  }

}
