import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
  constructor(private http:Http, private router:Router) { 
    
  }
  login(email:string, password:string){
    let data = {
      "email" : email,
      "password" : password
    };
    let body = JSON.stringify(data);
    let headers = new Headers({"content-type" : "application/json"});
    let options = new RequestOptions({headers: headers});

    this.http.post('http://localhost:8000/api/login',body,options)
    .subscribe(
      result => {
        localStorage.setItem('token', result.json().token);
        this.router.navigate(['/']);
      },
      err => {
        localStorage.removeItem('token');
        this.router.navigate(['/register']);
      }
    )
  }
  register(name:string, email:string, password:string){
    let data = {
      "name" : name,
      "email" : email,
      "password" : password
    };
    let body = JSON.stringify(data);
    let headers = new Headers({"content-type" : "application/json"});
    let options = new RequestOptions({headers: headers});

    this.http.post('http://localhost:8000/api/register',body,options)
    .subscribe(
      result => {
        localStorage.setItem('token', result.json().token);
        this.router.navigate(['/']);
      },
      err => {
        localStorage.removeItem('token');
        this.router.navigate(['/register']);
      }
    )
  }
  validate(){
    let token = localStorage.getItem('token');
    if(token = null){
      this.router.navigate(['login']);
    }
    else{
      
    }
  }
  fetch(){
    let token = localStorage.getItem('token');
    let headers = new Headers({"Authorization" : "Bearer" + token});
    let options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:8000/api/list',options)
    .map(result => result.json());
  }

}
