import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  name:string = '';
  email:string = '';
  password:string = '';

  constructor(private api:ApiService) { }

  ngOnInit() {
  }
  register(){
    this.api.register(this.name,this.email,this.password);
  }

}
